

import 'package:app/login_page.dart';
import 'package:flutter/material.dart';


void main()=>runApp(MyApp());
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(

        theme: new ThemeData(
        primarySwatch: Colors.red,
      ),
          
      title: 'KPC Queue Viewer',
      home: LoginPage(),
    );
    
  }
}
