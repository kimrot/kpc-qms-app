
import 'dart:collection';

import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';

import 'models.dart';

class QueuesPage extends StatefulWidget {
  final String loc;
   final String text;

   
 QueuesPage({this.loc,this.text});

  @override
  QueuesPageState createState() => QueuesPageState();
}
class QueuesPageState extends State<QueuesPage> {
  final _debouncer = Debouncer(milliseconds: 500);
   List<Queue>  items = List();
   List<Queue> filteredItems = List();

    String url;
   
    
 

  @override
  void initState() {
    super.initState();
     if(widget.loc == "Kisumu"){
      url ="https://qmskisumu.kpc.co.ke/handheld/broadqueue/${widget.text}";
    }
    if(widget.loc  =="Eldoret"){
      url = "https://qmseldoret.kpc.co.ke/handheld/broadqueue/${widget.text}";
    }
    if(widget.loc  =="Nakuru"){
      url ="https://qmsnakuru.kpc.co.ke/handheld/broadqueue/${widget.text}";
    }
      Servequeues.getQueues(url).then((itemsFromServer) {
      setState(() {
        items = itemsFromServer;
        filteredItems = items;
      });
    });
  }
  
  
  // List<Depots> depots= List();
  // List<Depots> filteredDepots = List();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Queues'),
      ),
      body: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextField(
              decoration: InputDecoration(
                labelText: "Search",
                      hintText: "Search",
                      prefixIcon: Icon(Icons.search),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(25.0)))
              ),
              onChanged: (string) {
                _debouncer.run(() {
                  setState(() {
                    filteredItems = items
                        .where((i) => (i.checkpoint
                                .toLowerCase()
                                .contains(string.toLowerCase()) ||
                            i.vehicleNumber.toLowerCase().contains(string.toLowerCase())||i.driverName
                                .toLowerCase()
                                .contains(string.toLowerCase())||i.omc
                                .toLowerCase()
                                .contains(string.toLowerCase())||i.queueNumber
                                .toLowerCase()
                                .contains(string.toLowerCase())))
                        .toList();
                  });
                });
              },
            ),
          ),
          Expanded(
            child: ListView.builder(
              padding: EdgeInsets.all(10.0),
              itemCount: filteredItems.length,
              itemBuilder: (BuildContext context, int index) {
                return Card(
              color: HexColor(filteredItems[index].color),
              elevation: 10,
              child: ListTile(
                isThreeLine: true,
                
                leading: Icon(Icons.queue, color: Colors.black,size: 30,),               
                title: Text("Queue Number: ${filteredItems[index].queueNumber}", 
                          style: TextStyle(
                          
                              fontSize: 20.0, fontWeight: FontWeight.bold),
                ),
                subtitle: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.all(4),
                    ),
                   
                    Text(filteredItems[index].checkpoint,
                          
                          style: TextStyle(fontSize: 18.0,fontWeight: FontWeight.bold,color: Colors.black)),
                    Text(filteredItems[index].vehicleNumber,
                          
                          style: TextStyle(fontSize: 14.0, fontWeight: FontWeight.bold,color: Colors.black),),
                 Text(filteredItems[index].omc,
                          
                          style: TextStyle(fontSize: 14.0,fontWeight: FontWeight.bold,color: Colors.black),),
                 Text(filteredItems[index].driverName,
                          
                          style: TextStyle(fontSize: 14.0, fontWeight: FontWeight.bold,color: Colors.black),),
                 Text(formatDate(DateTime.parse(filteredItems[index].time), [d, '-', M, '-', yyyy, ', ', HH, ':', nn]),
                          
                          style: TextStyle(fontSize:14.0, fontWeight: FontWeight.bold,color: Colors.black),)
                  ],
                ),
                
                contentPadding: EdgeInsets.all(10),  ),
            
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}