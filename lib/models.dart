


import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';

class Login {
    int status;
    String message;

    Login({
        this.status,
        this.message,
    });

    factory Login.fromJson(Map<String, dynamic> fromJson){
    return Login(
        status: fromJson["status"],
        message: fromJson["message"],
    );
   
}
}


class  Depots {
  int id;
  String code;
  String location;
 
  Depots({this.id, this.code, this.location});
 
  factory Depots.fromJson(Map<String, dynamic> json) {
    return Depots(
      id: json["id"] as int,
      code: json["code"] as String,
      location: json["location"] as String,
    );
  }
}

class BroadQueues{
final int status;
final String messages;
final List<Contents> content;

BroadQueues({this.status, this. messages, this.content});

factory BroadQueues.fromJson(Map<String, dynamic>fromJson){
  return BroadQueues(
    status:fromJson['status'],
    messages: fromJson['messages'],
    content: fromJson['content']
  );
}


 }
class Contents{
  final int id;
  final String products;
  final String criteria;
  final String orderType;
  final String loading;
  final String color;


  Contents({ this.color, this.id, this.products, this.criteria,this.loading, this.orderType});

  factory Contents.fromJson(Map<String, dynamic>fromJson){
    return Contents(
      products:fromJson['products'],
      criteria:fromJson['criteria'],
      orderType: fromJson['order_type'],
      loading: fromJson['loading'],
      id: fromJson['id'],
      color: fromJson['color']
    );
  }

}


class Servequeues{
  
   List<Queue>  items = List();

   

 static Future<List<Queue>> getQueues(String url) async{
    List<Queue> list = List();
  try{
    final response = await http.get(url);
    if (response.statusCode == 200) {
          var queues = json.decode(response.body);
          var queue = queues['content'] as List;
          list = queue.map<Queue>((json)=>Queue.fromJson(json)).toList();
             return list;        
    }
    
    else {
        throw Exception("Queues Not Found");
      }
    
  }catch (e) {
      throw Exception(e.toString());
    }
    
}

}

class Services {

 
   static Future<List<Depots>> getDepots(String url) async {
    
    try {
      final response = await http.get(url);
      if (response.statusCode == 200) {
        List<Depots> list = parseDepots(response.body);
        return list;
      } else {
        throw Exception("Depots Not Found");
      }
    } catch (e) {
      throw Exception(e.toString());
    }
  }
 
  static List<Depots> parseDepots(String responseBody) {
    final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
    return parsed.map<Depots>((json) => Depots.fromJson(json)).toList();
  } 
}

class Debouncer {
  final int milliseconds;
  VoidCallback action;
  Timer _timer;
 
  Debouncer({this.milliseconds});
 
  run(VoidCallback action) {
    if (null != _timer) {
      _timer.cancel();
    }
    _timer = Timer(Duration(milliseconds: milliseconds), action);
  }
}



 class Queues {
  final int status;
  final String message;
  final List<Queue> content;


  Queues({this.status,this.message, this.content});
  factory Queues.fromJson(Map<String, dynamic>fromJson){
    return Queues(
      status:fromJson['status'],
      message:fromJson['message'],
      content:fromJson['content'],
    );
  }
  
}

class Queue{
  final String queueNumber;
  final String orderNumber;
  final String checkpoint;
  final String color;
  final String driverName;
  final String omc;
  final String vehicleNumber;
  final String time;

  Queue({this.queueNumber, this.orderNumber, this.color, this.checkpoint, this.driverName, this.omc, this.time, this.vehicleNumber});
  
  factory Queue.fromJson(Map<String, dynamic>fromJson){
    print(fromJson);
    // time = new DateTime.now();
    return Queue(
      queueNumber:fromJson['queueNumber'],
      orderNumber: fromJson['orderNumber'],
      checkpoint: fromJson['checkpoint'],
      color: fromJson['color'],
      driverName: fromJson['driverName'],
      omc: fromJson['omc'],
      vehicleNumber: fromJson['vehicleNumber'],
      time: fromJson['time']['date']

    );
  }
}

class Time{
  final String time;
  

  Time({this.time});
  factory Time.fromJson(Map<String, dynamic>fromJson){
    // print(dynamic);
    return Time(
      time:fromJson['time']
    );
  }
}


class HexColor extends Color {
  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF" + hexColor;
    }
    return int.parse(hexColor, radix: 16);
  }

  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
}

