import 'package:app/broadqueues_page.dart';
import 'package:flutter/material.dart';


class QueueButtonPage extends StatelessWidget {

  final String text;
 QueueButtonPage({this.text});
 
   @override
  Widget build(BuildContext context) {  
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: true ,
        backgroundColor: Colors.red,
        title: Text('Order Type - $text'),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.vertical(bottom: Radius.circular(30)),
      ),
      ),
    
      body: Center(
        child: Container(
          
          child: Center(
            
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              
              children: [
                SizedBox(height: 20,),

                Card(
                  elevation: 10,

                child:ButtonTheme(
                  buttonColor: Colors.white,
                  height: 100,
                  minWidth: double.infinity,
                                  
                child: RaisedButton(
                  
                    child: Text("Local Orders",
                    style: TextStyle(
                      fontSize: 30,
                    ),),
                  
                    
                    textColor: Colors.black,
                    padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                    splashColor: Colors.black,
                     onPressed:(){
                 Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) => BroadQueuesPage(txt:'Local',location : text))
                 );
                    },
                    
                  ),
                )
                ),
               SizedBox(
                 height: 10,
                 ),
               Card(
                 elevation: 10.0,
                 
                              child: ButtonTheme(
                                buttonColor: Colors.white,
                                height: 100,
                    minWidth: double.maxFinite,             
                  child: RaisedButton(
                      child: Text("Export Orders",
                      style: TextStyle(
                        
                        fontSize: 30
                      ),),
                      
                      textColor: Colors.black,
                      padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                      splashColor: Colors.black,
                      onPressed:(){
                           Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) => BroadQueuesPage(txt:'Export',location : text))
                       );
                      },
                      
                    ),
                  ),
               ),
              ],
            ),
          ),
        ),
      ),
    
    );
  }
}