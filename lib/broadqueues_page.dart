import 'dart:convert';

import 'package:app/queues_page.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'models.dart';

class BroadQueuesPage extends StatelessWidget {
   final String txt;
   final String location;
 BroadQueuesPage({this.txt, this.location});


  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      appBar: AppBar(
        centerTitle: true ,
        title: Text('Order types - $location'),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.vertical(bottom: Radius.circular(30)),
      ),
      ),
      body: Center(
        
          child: _futureWidget(),
          
        
      ),
    );
  }

   _futureWidget() {
    List<Contents> list;
    List<Contents> localOrders = List();
    List<Contents> exportOrders= List();
     String url;
    if(location == "Kisumu"){
      url ="https://qmskisumu.kpc.co.ke/handheld/broadqueues";
    }
    if(location =="Eldoret"){
      url = "https://qmseldoret.kpc.co.ke/handheld/broadqueues";
    }
    if(location =="Nakuru"){
      url ="https://qmsnakuru.kpc.co.ke/handheld/broadqueues";
    }
    if (txt=="Local"){
    return FutureBuilder(
      future: http.get(url),
      
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          var orders = json.decode(snapshot.data.body);
          print("These are the orders@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@$orders");
          var conts = orders['content'] as List;
          list = conts.map<Contents>((json)=>Contents.fromJson(json)).toList();
                   
            
    

          localOrders.clear();
           for(var i in list){
                  if (i.orderType == "Local") {
                    localOrders.add(i);
                  }
              }
              return _buildorders(context, localOrders);          
        } else {
          return Center(
            child: CircularProgressIndicator(),
          );
          
        }
      },
    );
  }
  if (txt=="Export"){
    return FutureBuilder(
      future: http.get(url),
      
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          var orders = json.decode(snapshot.data.body);
          var conts = orders['content'] as List;
          list = conts.map<Contents>((json)=>Contents.fromJson(json)).toList();
           
          
                    


          localOrders.clear();
           for(var i in list){
                  if (i.orderType == "Export") {
                    exportOrders.add(i);
                  }
              }
              return _buildorders(context, exportOrders);          
        } else {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
      },
    );
  }

  }
  

  ListView _buildorders(BuildContext context, List orders){
    return ListView.builder( 
      itemCount: orders.length,
      
      itemBuilder: (contex, index){
        
        return Container(
          
          child: Center(
            
            child: Card(
              color: Colors.white,
              elevation: 10,
              child: ListTile(
                isThreeLine: true,
                
                leading: Icon(Icons.queue, color: Colors.black,size: 50,),
                trailing: Icon(Icons.keyboard_arrow_right, color: Colors.black,size: 50,),
                title: Text(orders[index].products, 
                style: TextStyle(fontSize:25.0,fontWeight: FontWeight.w800, color: Colors.black),
                
                ),
                subtitle: Column(
                  children: <Widget>[
                    Text(orders[index].criteria,
                style: TextStyle(fontSize:16.0,fontWeight: FontWeight.w800, color: Colors.red)),
                    Text(orders[index].loading,
                style: TextStyle(fontSize:16.0,fontWeight: FontWeight.w800, color: Colors.blue))
                  ],
                ),
                  
                contentPadding: EdgeInsets.all(10),

                
                onTap: (){
                 Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) => QueuesPage(loc : location, text : orders[index].id.toString()))
                );

                },
                
              ),
            ),
          ),
        );
      }
    );
  }
  
}