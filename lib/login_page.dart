import 'dart:async';
import 'dart:convert';
//import 'package:flushbar/flushbar.dart';
import 'package:app/depots_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
// import 'package:shared_preferences/shared_preferences.dart';
import 'models.dart';

class LoginPage extends StatefulWidget {

  
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
BuildContext _scaffoldContext;
  bool _isLoading = false;
  String value = "";
  
 
  @override
  Widget build(BuildContext context) {
    
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light.copyWith(statusBarColor: Colors.transparent));
    return SafeArea(
      
          child: Scaffold(
                   
        body: Builder(
          builder: (BuildContext context) {
            _scaffoldContext = context;
            return Center(
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
              ),
              child:_isLoading ? Center(child: CircularProgressIndicator()) : ListView(
                children: <Widget>[
                  
                  SizedBox(height: 110,),
                  headerSection(),
                  SizedBox(height: 50,),
                  textSection(),
                  SizedBox(height: 10,),
                  buttonSection(),
                ],
                
              ),
            ),
          );
          }
        ),
      ),
    );
  }

 Future <Login> _signIn(String url) async {
    
     var jsonResponse;
    var response = await http.post(url,);
    if(response.statusCode == 200) {
      jsonResponse = json.decode(response.body);
       var conts = jsonResponse['status'];
        if(conts == 0) {
       
        Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (BuildContext context) => DepotsPage()), (Route<dynamic> route) => false);
      }else if(conts == 1){

     showDefaultSnackbar(context);
      
      }
    }
    
    return _signIn(url);
  }

 

  Container buttonSection() {
    
    return Container(
      
      width: MediaQuery.of(context).size.width,
      height: 50.0,
      padding: EdgeInsets.symmetric(horizontal: 10.0),
      margin: EdgeInsets.only(top: 15.0),
      child: RaisedButton(
        
        onPressed: emailController.text == "" || passwordController.text == "" ? null: () {
          setState(() {
            _isLoading = true;            
          });
          
           final List<String> urls = ['https://qmseldoret.kpc.co.ke/apis/login?username=${emailController.text}&password=${passwordController.text}',
          'https://qmsnakuru.kpc.co.ke/apis/login?username=${emailController.text}&password=${passwordController.text}',
          'https://qmskisumu.kpc.co.ke/apis/login?username=${emailController.text}&password=${passwordController.text}'];
         for(var i in urls){
          _signIn(i);
         }

         
         
        },

        elevation: 0.0,
        color: Colors.red,
        child: Text("Sign In", style: TextStyle(color: Colors.black87,fontSize: 18, fontWeight: FontWeight.bold)),
        
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
      ),
    );
  }

  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  

  Container textSection() {
    SizedBox(height: 50,);
    return Container(
     padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 20.0),
      child: Column(
        
        

        
        children: <Widget>[
          TextField( 
             onChanged: (text) {
                 value = text;
               },

            controller: emailController,
            cursorColor: Colors.red,
            
            
            style: TextStyle(color: Colors.black87),
            decoration: InputDecoration(
              contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
              prefixIcon: Icon(Icons.email, color: Colors.black87),
              hintText: "Username",
              labelText: "Username",
              border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(25.0))),
              hintStyle: TextStyle(color: Colors.black87),
            ),
          ),
          SizedBox(height: 30.0),
          TextField(         
            controller: passwordController,
            cursorColor: Colors.red,
            obscureText: true,
            style: TextStyle(color: Colors.black87),
            decoration: InputDecoration(
              contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
              prefixIcon: Icon(Icons.lock, color: Colors.black87),
              hintText: "Password",
              labelText: "Password" ,
              border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(25.0))),
              hintStyle: TextStyle(color: Colors.black87),
            ),
          ),
        ],
      ),
    );
  }

  Container headerSection() {
    return Container(
      
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          CircleAvatar(
            radius: 70.0,
            backgroundImage:AssetImage('assets/pipu.jpg')
          ),
        ],
      ),
      
    );
    
  }

  
   void showDefaultSnackbar(BuildContext context) {
    Scaffold.of(_scaffoldContext)
    
    .showSnackBar(
      SnackBar(
        duration: Duration(seconds: 4),
        
        content: Text('Please enter correct credentials'),
           ),
    );
    
    

    
  }
 
}
