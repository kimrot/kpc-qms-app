
import 'login_page.dart';
import 'package:app/order_type_buttonsPage.dart';
import 'package:flutter/material.dart';
import 'models.dart';

class DepotsPage extends StatefulWidget {
  DepotsPage() : super(); 

  final String title = "Select your Depot";

  @override
  DepotsPageState createState() => DepotsPageState();
}
class DepotsPageState extends State<DepotsPage> {

 

  @override
  void initState() {
    super.initState();
      Services.getDepots(url).then((depotsFromServer) {
      setState(() {
        
        depots = depotsFromServer;
        filteredDepots = depots;
        
      });
    });
  }
  String url = 'https://qmseldoret.kpc.co.ke/handheld/depots';
  
  List<Depots> depots= List();
  List<Depots> filteredDepots = List();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          FlatButton(
      textColor: Colors.white,
      onPressed: () {
          Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) => LoginPage()));
      },
      child: Text("Logout"),
      
    ),
        ],
        centerTitle: true ,
        title: Text(widget.title),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.vertical(bottom: Radius.circular(30)),
      ),
      ),
      body: Column(
        children: <Widget>[         
          Expanded(
            child: ListView.builder(
              padding: EdgeInsets.all(10.0),
              itemCount: filteredDepots.length,
              itemBuilder: (BuildContext context, int index) {
                return Card(
                       color: Colors.white,
                       elevation: 8,
                  child: Padding(
                    padding: EdgeInsets.all(10.0),
                    child: ListTile(                
                        leading: Icon(Icons.location_on, color: Colors.red,size: 50,),
                        trailing: Icon(Icons.keyboard_arrow_right, color: Colors.black,size: 50,),
                        title: Text(filteredDepots[index].location, 
                          style: TextStyle(fontSize:30.0,fontWeight: FontWeight.w800, color: Colors.black),),
                        subtitle: Text(filteredDepots[index].code,
                        style: TextStyle(fontSize:25.0,fontWeight: FontWeight.w800, color: Colors.red),),                        
                        contentPadding: EdgeInsets.all(10),
                        
                        onTap: (){
                          Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) => QueueButtonPage(text:filteredDepots[index].location))
                        );

                        },  
                    ),
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}